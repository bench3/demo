package com.example.demo.controller;

import com.example.demo.dto.CatDTO;
import com.example.demo.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.io.IOException;
import java.math.BigInteger;
import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.*;

@RestController
public class HelloController {

    @Autowired
    private HelloService helloService;
    private  final ExecutorService executor = Executors.newFixedThreadPool(2);


    @GetMapping("/")
    public Map<String, String> get() {
        return Map.of("message", "hello");
    }

    @GetMapping("/uuid")
    public UUID getUuid() {
        return UUID.randomUUID();
    }

    @GetMapping("/crypto")
    public String getCrypto() throws InvalidKeySpecException, NoSuchAlgorithmException {
        int iterations = 10_000;
        String pass = "mot_de_passe";
        char[] chars = pass.toCharArray();
        byte[] salt = getSalt();

        PBEKeySpec spec = new PBEKeySpec(chars, salt, iterations, 512);
        SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");

        byte[] hash = skf.generateSecret(spec).getEncoded();

        return toHex(hash);
    }

    private byte[] getSalt() throws NoSuchAlgorithmException
    {
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
        byte[] salt = new byte[128];
        sr.nextBytes(salt);
        return salt;
    }

    private String toHex(byte[] array) throws NoSuchAlgorithmException
    {
        BigInteger bi = new BigInteger(1, array);
        String hex = bi.toString(16);

        int paddingLength = (array.length * 2) - hex.length();
        if(paddingLength > 0)
        {
            return String.format("%0"  +paddingLength + "d", 0) + hex;
        }else{
            return hex;
        }
    }

    @GetMapping("/loginParallel")
    public Map<String, Object> loginParallel() throws URISyntaxException, IOException, InterruptedException, ExecutionException {
        String [] tags= helloService.getTags();
        CatDTO [] cutes;
        CatDTO cat;

        Future<CatDTO[]> f1 = executor.submit(new Callable<CatDTO[]>() {
            @Override
            public CatDTO[] call() throws URISyntaxException, IOException, InterruptedException {
                return helloService.getCuteTag();
            }
        });

        Future<CatDTO> f2 = executor.submit(new Callable<CatDTO>() {
            @Override
            public CatDTO call() throws URISyntaxException, IOException, InterruptedException {
                return helloService.getCat();
            }
        });

        cutes = f1.get();
        cat = f2.get();

        return Map.of("tags", tags, "cutes", cutes, "cat", cat);
    }

    @GetMapping("/login")
    public Map<String, Object> login() throws URISyntaxException, IOException, InterruptedException, ExecutionException {
        String [] tags= helloService.getTags();
        CatDTO [] cutes = helloService.getCuteTag();
        CatDTO cat = helloService.getCat();
        return Map.of("tags", tags, "cutes", cutes, "cat", cat);
    }
}
