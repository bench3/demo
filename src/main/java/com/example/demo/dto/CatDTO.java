package com.example.demo.dto;

public class CatDTO {
    public String[] tags;
    public String createdAt;
    public String updatedAt;
    public boolean validated;
    public String owner;
    public String file;
    public String mimetype;
    public int size;
    public String _id;
    public String url;
}
