package com.example.demo.service;

import com.example.demo.dto.CatDTO;
import com.google.gson.Gson;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;

@Service
public class HelloService {

    private final HttpClient client = HttpClient.newBuilder().build();
    private final Gson gson = new Gson();

    public String[] getTags() throws URISyntaxException, IOException, InterruptedException {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(new URI("https://cataas.com/api/tags"))
                .GET()
                .build();
        HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
        return gson.fromJson(response.body(), String[].class);
    }

    public CatDTO getCat() throws URISyntaxException, IOException, InterruptedException {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(new URI("https://cataas.com/cat?json=true"))
                .GET()
                .build();
        HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
        return gson.fromJson(response.body(), CatDTO.class);
    }

    public CatDTO[] getCuteTag() throws URISyntaxException, IOException, InterruptedException {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(new URI("https://cataas.com/api/cats?tags=cute"))
                .GET()
                .build();
        HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
        return gson.fromJson(response.body(), CatDTO[].class);
    }
}
